python-xstatic-jquery (3.5.1.1-4) unstable; urgency=medium

  * Removed -O--buildsystem=python_distutils from d/rules.

 -- Thomas Goirand <zigo@debian.org>  Fri, 20 Dec 2024 11:18:27 +0100

python-xstatic-jquery (3.5.1.1-3) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090664).

 -- Thomas Goirand <zigo@debian.org>  Wed, 18 Dec 2024 16:23:08 +0100

python-xstatic-jquery (3.5.1.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Mon, 02 Oct 2023 16:13:13 +0200

python-xstatic-jquery (3.5.1.1-1) experimental; urgency=medium

  * New upstream release.
  * Rebased no-privacy-breach.patch.

 -- Thomas Goirand <zigo@debian.org>  Fri, 01 Sep 2023 09:45:28 +0200

python-xstatic-jquery (1.12.4.1-3) unstable; urgency=medium

  * Cleans better (Closes: #1047555).

 -- Thomas Goirand <zigo@debian.org>  Mon, 21 Aug 2023 13:38:24 +0200

python-xstatic-jquery (1.12.4.1-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 18 Jul 2019 23:54:18 +0200

python-xstatic-jquery (1.12.4.1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Removed "Commands not to run" in debian/rules.

 -- Thomas Goirand <zigo@debian.org>  Tue, 02 Apr 2019 17:28:07 +0200

python-xstatic-jquery (1.10.2.1-4) unstable; urgency=medium

  * Removed Python 2 support.
  * Add missing dh-python build-depends.
  * Fixed long desc.
  * Standards-Version is now 4.1.3.

 -- Thomas Goirand <zigo@debian.org>  Mon, 02 Apr 2018 14:52:11 +0200

python-xstatic-jquery (1.10.2.1-3) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Thomas Goirand ]
  * Fixed python3 package depends on python2 xstatic.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2018 01:13:13 +0100

python-xstatic-jquery (1.10.2.1-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 02 Nov 2017 13:22:24 +0000

python-xstatic-jquery (1.10.2.1-1) experimental; urgency=medium

  [ Daniel Baumann ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Updating maintainer field.
  * Running wrap-and-sort -bast.
  * Updating standards version to 4.0.0.
  * Removing gbp.conf, not used anymore or should be specified in the
    developers dotfiles.
  * Updating standards version to 4.0.1.
  * Updating standards version to 4.1.0.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed debian/copyright ordering and years.

 -- Thomas Goirand <zigo@debian.org>  Sat, 14 Oct 2017 10:50:38 +0200

python-xstatic-jquery (1.7.2.0+dfsg1-4) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).
  * d/s/options: extend-diff-ignore of .gitreview
  * d/control: Using OpenStack's Gerrit as VCS URLs.
  * Bumped debhelper compat version to 10

  [ Thomas Goirand ]
  * Not using libjs-jquery which bumped to a too high version.
  * Clean-up (build-)dependencies.
  * Standards-Version: 3.9.8.
  * Removed not-active uploaders.
  * Using pkgos-dh_auto_install from openstack-pkg-tools.
  * Add jquery itself.

 -- Thomas Goirand <zigo@debian.org>  Wed, 30 Nov 2016 16:25:23 +0100

python-xstatic-jquery (1.7.2.0+dfsg1-3) unstable; urgency=medium

  * Removes the .pth files from binary.
  * Added a watch file.

 -- Thomas Goirand <zigo@debian.org>  Wed, 01 Oct 2014 06:47:08 +0000

python-xstatic-jquery (1.7.2.0+dfsg1-2) unstable; urgency=medium

  * Added missing depends and build-conflicts on python{3,}-xstatic.

 -- Thomas Goirand <zigo@debian.org>  Sun, 21 Sep 2014 00:18:28 +0800

python-xstatic-jquery (1.7.2.0+dfsg1-1) unstable; urgency=medium

  * Initial release. (Closes: #756734)

 -- Thomas Goirand <zigo@debian.org>  Sat, 28 Jun 2014 17:16:08 +0800
